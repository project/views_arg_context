<?php
/**
 * @file
 * Contains the active context argument default plugin.
 */

/**
 * Default argument plugin to extract a value from the active context
 */
class views_plugin_argument_default_context extends views_plugin_argument_default {
  function option_definition() {
    $options = parent::option_definition();
    $options['namespace'] = array('default' => '');
    $options['attribute'] = array('default' => '');

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    $form['namespace'] = array(
      '#type' => 'textfield',
      '#title' => t('Namespace'),
      '#default_value' => $this->options['namespace'],
      );
	$form['attribute'] = array(
      '#type' => 'textfield',
      '#title' => t('Attribute'),
      '#default_value' => $this->options['attribute'],
      );
  }

  function get_argument() {
    return context_get($this->options['namespace'], $this->options['attribute']);
  }
}